package com.artur.geo.service;

import com.artur.geo.database.FakeDatabase;
import com.artur.geo.model.Batch;
import com.artur.geo.model.Location;
import com.artur.geo.model.UserLocation;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.TreeMap;

import static com.google.common.collect.Lists.newArrayList;
import static java.lang.Double.parseDouble;

@Service
public class GeoService {

    private FakeDatabase database;

    GeoService(FakeDatabase database) {
        this.database = database;
    }

    public Integer updateLocations(Batch batch) {
        database.setLocations(batch.getLocations());
        return batch.getLocations().size();
    }

    public List<String> getNearestLocation(UserLocation userLocation) {
        TreeMap<Double, List<String>> computedLocations = new TreeMap<>();

        database.getLocations()
                .forEach(location -> {
                    double distance = getDistanceFromUser(location, userLocation);

                    if (computedLocations.containsKey(distance)) {
                        computedLocations.get(distance).add(location.getName());
                    } else {
                        computedLocations.put(distance, newArrayList(location.getName()));
                    }
                });

        return computedLocations.firstEntry().getValue();
    }

    private double getDistanceFromUser(Location location, UserLocation userLocation) {
        double latitude = parseDouble(location.getLatitude());
        double longitude = parseDouble(location.getLongitude());

        double userLatitude = parseDouble(userLocation.getLatitude());
        double userLongitude = parseDouble(userLocation.getLongitude());

        if (latitude == userLatitude && longitude == userLongitude) {
            return 0d;
        } else {
            double theta = longitude - userLongitude;
            double distance = Math.sin(Math.toRadians(latitude)) * Math.sin(Math.toRadians(userLatitude)) + Math.cos(Math.toRadians(latitude)) * Math.cos(Math.toRadians(userLatitude)) * Math.cos(Math.toRadians(theta));

            distance = Math.acos(distance);
            distance = Math.toDegrees(distance);
            distance = distance * 60 * 1.1515;

            return distance;
        }
    }
}
