package com.artur.geo.database;

import com.artur.geo.model.Location;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FakeDatabase {

    private List<Location> locations;

    public FakeDatabase() {
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }
}
