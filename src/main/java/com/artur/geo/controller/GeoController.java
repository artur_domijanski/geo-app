package com.artur.geo.controller;

import com.artur.geo.model.Batch;
import com.artur.geo.model.UserLocation;
import com.artur.geo.service.GeoService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static java.lang.String.format;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@RestController
@RequestMapping(path = "/geo")
public class GeoController {

    private GeoService geoService;

    public GeoController(GeoService geoService) {
        this.geoService = geoService;
    }

    @PutMapping(value = "/location")
    public ResponseEntity<String> update(@RequestBody @Valid Batch batch) {
        return ResponseEntity.status(CREATED)
                .contentType(APPLICATION_JSON)
                .body(format("Updated %d locations.", geoService.updateLocations(batch)));
    }

    @PostMapping(value = "/user")
    public ResponseEntity<String> get(@RequestBody @Valid UserLocation userLocation) {
        return ResponseEntity.status(OK)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(format("The nearest location from user is: %s", geoService.getNearestLocation(userLocation)));
    }
}
