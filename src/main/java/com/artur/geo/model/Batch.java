package com.artur.geo.model;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

public class Batch {

    @NotEmpty(message = "List of locations cannot be empty.")
    @Valid
    private List<Location> locations;

    private Batch(List<Location> locations) {
        this.locations = locations;
    }

    public Batch() {
    }

    public List<Location> getLocations() {
        return locations;
    }

    public static class Builder {

        private List<Location> locations;

        public Builder() {
            locations = new ArrayList<>();
        }

        public Builder withLocation(String name, String latitude, String longitude) {
            locations.add(new Location(name, latitude, longitude));
            return this;
        }

        public Batch build() {
            return new Batch(locations);
        }
    }
}
