package com.artur.geo.model;

import javax.validation.constraints.NotBlank;

public class UserLocation {

    @NotBlank(message = "Latitude cannot be empty.")
    private String latitude;

    @NotBlank(message = "Longitude cannot be empty.")
    private String longitude;

    public UserLocation(String latitude, String longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public UserLocation() {
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }
}
