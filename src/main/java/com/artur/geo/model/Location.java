package com.artur.geo.model;

import javax.validation.constraints.NotBlank;

public class Location {

    @NotBlank(message = "Name cannot be empty.")
    private String name;

    @NotBlank(message = "Latitude cannot be empty.")
    private String latitude;

    @NotBlank(message = "Longitude cannot be empty.")
    private String longitude;

    Location(String name, String latitude, String longitude) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Location() {
    }

    public String getName() {
        return name;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }
}
