package com.artur.geo;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import static org.springframework.boot.SpringApplication.run;

@SpringBootApplication
public class GeoAppApplication {

    public static void main(String[] args) {
        run(GeoAppApplication.class, args);
    }
}