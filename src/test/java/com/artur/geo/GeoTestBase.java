package com.artur.geo;

import com.artur.geo.model.Batch;

public class GeoTestBase {

    protected static final String FIRST_NAME = "first";
    protected static final String SECOND_NAME = "second";
    protected static final String THIRD_NAME = "third";

    protected static final String FIRST_LATITUDE = "1";
    protected static final String SECOND_LATITUDE = "1";
    protected static final String THIRD_LATITUDE = "1";

    protected static final String FIRST_LONGITUDE = "3";
    protected static final String SECOND_LONGITUDE = "3.1";
    protected static final String THIRD_LONGITUDE = "2.9";

    protected Batch createBatch() {
        return new Batch.Builder()
                .withLocation(FIRST_NAME, FIRST_LATITUDE, FIRST_LONGITUDE)
                .withLocation(SECOND_NAME, SECOND_LATITUDE, SECOND_LONGITUDE)
                .withLocation(THIRD_NAME, THIRD_LATITUDE, THIRD_LONGITUDE)
                .build();
    }
}
