package com.artur.geo.controller;

import com.artur.geo.GeoTestBase;
import com.artur.geo.model.UserLocation;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class GeoControllerTest extends GeoTestBase {

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void update() throws Exception {
        mockMvc.perform(put("/geo/location")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createBatch())))
                .andExpect(status().isCreated())
                .andExpect(content().string("Updated 3 locations."));
    }

    @Test
    public void get() throws Exception {
        UserLocation userLocation = new UserLocation("111", "222");

        mockMvc.perform(post("/geo/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userLocation)))
                .andExpect(status().isOk())
                .andExpect(content().string("The nearest location from user is: [second]"));
    }
}