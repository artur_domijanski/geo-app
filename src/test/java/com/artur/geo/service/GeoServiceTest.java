package com.artur.geo.service;

import com.artur.geo.GeoTestBase;
import com.artur.geo.database.FakeDatabase;
import com.artur.geo.model.Location;
import com.artur.geo.model.UserLocation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class GeoServiceTest extends GeoTestBase {

    private FakeDatabase database;
    private GeoService geoService;

    @Before
    public void setUp() {
        database = new FakeDatabase();
        geoService = new GeoService(database);

        assertThat(database.getLocations(), is(nullValue()));
    }

    @Test
    public void updateLocationsShouldStoreLocation() {
        int size = geoService.updateLocations(createBatch());

        assertThat(size, is(3));
        assertThat(database.getLocations(), is(notNullValue()));

        List<Location> locations = database.getLocations();

        assertThat(locations.size(), is(3));
        assertLocation(locations.get(0), FIRST_NAME, FIRST_LATITUDE, FIRST_LONGITUDE);
        assertLocation(locations.get(1), SECOND_NAME, SECOND_LATITUDE, SECOND_LONGITUDE);
        assertLocation(locations.get(2), THIRD_NAME, THIRD_LATITUDE, THIRD_LONGITUDE);
    }

    @Test
    public void getNearestLocationGivesCorrectLocation() {
        database.setLocations(createBatch().getLocations());

        List<String> name = geoService.getNearestLocation(new UserLocation("1", "3"));

        assertThat(name.size(), is(1));
        assertThat(name.get(0), is(FIRST_NAME));
    }

    @Test
    public void getNearestLocationGivesCorrectLocation2() {
        database.setLocations(createBatch().getLocations());

        List<String> name = geoService.getNearestLocation(new UserLocation("1", "2.9"));

        assertThat(name.size(), is(1));
        assertThat(name.get(0), is(THIRD_NAME));
    }

    @Test
    public void getNearestLocationGivesCorrectLocationWithSameLocationDistances() {
        database.setLocations(createBatch().getLocations());

        List<String> name = geoService.getNearestLocation(new UserLocation("1", "3.05"));

        assertThat(name.size(), is(2));
        assertThat(name.get(0), is(FIRST_NAME));
        assertThat(name.get(1), is(SECOND_NAME));
    }

    private void assertLocation(Location location, String name, String latitude, String longitude) {
        assertThat(location.getName(), is(name));
        assertThat(location.getLatitude(), is(latitude));
        assertThat(location.getLongitude(), is(longitude));
    }
}